module.exports = {
  port: 1337,
  awsRegion: 'us-west-1',
  awsBucket: 'buytwitter',
  awsCache: '/tmp/buytwitter.cache',
  cloudFrontDistID: 'E3RE4PE1L3QGIG',
  title: '#WeAreTwitter #BuyTwitter',
  mainlogo: 'chick.png',
  embedPetition: {
    type: 'actionnetwork',
    id: 'wearetwitter'
  },
  meta: {
    "og:title": 'We can democratize Twitter',
    "og:url": 'https://www.buytwitter.org/',
    "og:description": 'We are users and shareholders telling Twitter to study an exit from Wall Street to democratic ownership. Join us!',
    "og:image": 'https://www.buytwitter.org/img/wearetwitter-bg.png',
    "twitter:site": '@BuyTwitter'
  },
  sections: {
    embedPetition: true,
    socialShare: true
  },
  twitter: {
    url: encodeURIComponent('https://www.buytwitter.org/'),
    text: encodeURIComponent("Let's #BuyTwitter and save our public utility! Tell $TWTR to study an exit to democratic ownership! buytwitter.org @BuyThisPlatform"),
    related: 'BuyThisPlatform',
  },
  fb: {
    app_id: '573217829552127',
    pagetitle: 'WeAreTwitter',
    ui_href: 'https://www.buytwitter.org/',
    href: 'https://www.facebook.com/xxxxxx/'
  },
  google: {
    analytics: {
      id: 'UA-54189940-10'
    }
  },
  pug: {
    html: true,
  }
}
