source .env
certbot --agree-tos -a certbot-s3front:auth \
--config-dir=aws/certbot/config/ \
--work-dir=aws/certbot/work/ \
--logs-dir=aws/certbot/logs/ \
--certbot-s3front:auth-s3-bucket buytwitter \
--certbot-s3front:auth-s3-region us-west-1 \
--certbot-s3front:auth-s3-directory "" \
--installer certbot-s3front:installer \
--certbot-s3front:installer-cf-distribution-id E3RE4PE1L3QGIG \
-d www.buytwitter.org \
--renew-by-default --text