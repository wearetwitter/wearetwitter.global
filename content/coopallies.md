# Co-op Allies Support the Proposal to \#BuyTwitter

Dear Twitter shareholders,

We believe you can learn from co-ops and all enterprises that thrive with democratic ownership.

Our organizations work with financially healthy and sustainable cooperatives that range from grocery stores and robotics firms, to electric co-ops with billions of dollars in revenue and credit unions with trillions of dollars in member deposits in the U.S. alone. Many successful examples have been around for decades and centuries, and hold valuable lessons for networked enterprises like Twitter.

Twitter has an enormous opportunity to boost user loyalty and deliver shareholder value. CEO Jack Dorsey calls Twitter the “people’s news network,” and retail shareholders account for 60% of its ownership. But the current business model fails to empower users as true stakeholders in Twitter’s future. 

*We believe that Twitter would benefit from studying a transition to democratic user ownership.*

That is why we support voting YES on the [the BuyTwitter proposal](http://buytwitter.org/proposal).

Moving toward a more sustainable structure could in turn reward current shareholders for their investment. Because cooperatives are bound by their governing bylaws to fulfill the interests of their members, they are strongly incentivized to provide them with the highest quality product. We see scenarios through which all stakeholders can win a better service and greater value. 

When you consider the proposal to study broad-based ownership and accountability on Twitter, we urge you to take our support into consideration.

As a coalition of organizations representing and supporting cooperative enterprises, we call on our allies to sign on to this letter, below.

Signed,

* David Hammer, Executive Director, the [Industrial Cooperative Association Group](http://ica-group.org/)
* William Azaroff, Executive Lead, [Vancity Credit Union](https://www.vancity.com/), and Board Chair, [Modo Co-operative](http://modo.coop/)
* Brendan Denovan, Communications Manager, [Co-operatives and Mutuals Canada](http://canada.coop/)
* Holly Fearing, Social Media Advisor, [Filene Research Institute](https://filene.org/) and Board Chair, [Willy Street Co-op](https://www.willystreet.coop/)
* Francesca Pick, Co-founder, [OuiShare Fest](http://ouisharefest.com/)
* Ed Mayo, Secretary General, [Co-operatives UK](https://www.uk.coop/)
* Maira Sutton, Community Engagement Manager, [Shareable](http://www.shareable.net/)
* Joel Brock, Member-Owner, the [Tech Support Cooperative](http://site.techsupport.coop/)
* Adam Schwartz, Member-Owner, [CDS Consulting Co-op](http://www.cdsconsulting.coop/)
* Jake Schlachter, Executive Director, [We Own It](http://weown.it)
* Judy Ziewacz, President & CEO, [National Cooperative Business Association (NCBA CLUSA)](http://ncba.coop/)
* Therese Tuttle, Principal, [Tuttle Law Group](http://www.cooplawgroup.com/)
* John Katovich, President, [Cutting Edge Capital](https://www.cuttingedgecapital.com/) and [Cutting Edge Counsel](https://cuttingedgecounsel.com/)
* Andrew Stachiw, Worker-Owner, [TESA Collective](http://www.toolboxfored.org/)
* Jenifer Daniels, APR, Founder & Managing Director, [Colorstock](https://getcolorstock.com/)
* Joseph Riemann, [Cooperative Principal](http://thecp.coop/)
* Don Jamison, Executive Director, [Vermont Employee Ownership Center](http://www.veoc.org/)
* Stephanie Guico, Spokesperson, [Caisse d'économie solidaire](https://www.caissesolidaire.coop/)
* Donnie Maclurcan, Executive Director, [Post Growth Institute](http://postgrowth.org)
* Christina Jennings, Executive Director, [Shared Capital Cooperative](http://sharedcapital.coop)
* Noemi Giszpenc, Executive Director, [Cooperative Development Institute](http://cdi.coop)
* James McRitchie, Shareholder Advocate, [CorpGov.net](http://corpgov.net)
* Betsy Avila, Executive Director, [Democracy at Work](http://democracyatwork.info)
* Shawn Berry, Worker-Owner, [LIFT Economy](http://lifteconomy.com)
* Alpen Sheth, Co-Founder, [Economic Space Agency](http://ecsa.io/)
* Tere Vaden, Managing Director, [Robin Hood Cooperative](http://www.robinhoodcoop.org/)
* Ricardo S. Nuñez, Director of Economic Democracy, [Sustainable Economies Law Center](http://www.theselc.org/)
* Francyne Morin, [Conseil québécois de la coopération et de lamutualité](https://www.cqcm.coop/)
* Jonathan Rodríguez Burgos, Cooperative Development Director, [LIGA de Cooperativas de Puerto Rico](http://www.liga.coop)
* Melissa Hoover, Executive Director, [Democracy at Work Institute](http://www.institute.coop)
* Kaeleigh Barker, Communications, [Cooperatives for a Better World](https://cooperativesforabetterworld.coop/)
* James Slezak, Managing Director, [New Economy Lab](http://neweconomylab.com/)
* Allison Basile, Co-Founder, [Tightshift Laboring Cooperative](http://www.tightshift.com/)
* Athina Karatzogianni, Media and Communication, [University of Leicester](https://le.ac.uk/) 
* Richard Bartlett, Co-Founder, [Loomio](https://www.loomio.org/)
* Laura Yu, Director, [International Rescue Committee](https://www.rescue.org/)
* Ann Marie Utratel, Advocacy, [P2P Foundation](https://blog.p2pfoundation.net/)
* Thomas Beckett, Executive Director, [Carolina Common Enterprise](http://www.commonenterprise.coop/)
* Trebor Scholz, Director, [Platform Cooperativism Consortium](https://platform.coop/)
* Rory Ridley-Duff, Co-Founder, [FairShares Association](http://www.fairshares.coop/)
* Janusz Paszkowski, [National Auditing Union of Workers’ Cooperatives (NAUWC)](http://www.zlsp.org.pl/pl/)
* Audra Krueger, Executive Director, [Co-operatives First](https://www.cooperativesfirst.com/)
* Marc Picard, General Director, [Caisse d’économie solidaire Desjardins](http://www.caissesolidaire.coop/)
* Jennifer Rau, Operational Leader, [Sociocracy For All](http://www.sociocracyforall.org/)
* Howard Brodsky, CEO, [CCA Global Partners](http://www.ccaglobalpartners.com/)
* Robert Matney, Co-Owner and COO, [Polycot Associates](http://polycotassociates.com)
* Esteban Kelly, Executive Director, [US Federation of Worker Co-ops](http://www.usworker.coop)

