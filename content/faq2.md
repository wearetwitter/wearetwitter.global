1\. Democratic ownership has a proven record of profitability and sustainability.

2\. A co-op or hybrid would benefit Twitter users and shareholders.

3\. Studying models for user ownership will show how current shareholders stand to gain.

4\. To evaluate opportunities, Twitter can engage a range of experts in democratic ownership.

5\. This proposal emerged from organizing among Twitter shareholders and users.

[Read more](/whydemocratize/){class="btn btn-block btn-lg btn-twtr centered"}
