
/* ========================================================================
 * BuyTwitter.org 
 * ========================================================================*/

/* countdown clock */

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      daysSpan.innerHTML = 0;
      hoursSpan.innerHTML = 0;
      minutesSpan.innerHTML = 0;
      secondsSpan.innerHTML = 0;
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

function doOpenFbShare() {
  FB.ui({
    method: 'share',
    href: '<%= fb.ui_href %>',
  }, function(response){});
  ga('send', 'event', 'ShareFacebook', 'click', 'Facebook Share');
}

function doTrackTwitterShare() {
  ga('send', 'event', 'ShareTwitter', 'click', 'Twitter Share');
}

function addCommas(num){
  var num_str = num.toString();
    var offset = num_str.length % 3;
    if (offset == 0)
        return num_str.substring(0, offset) + num_str.substring(offset).replace(/([0-9]{3})(?=[0-9]+)/g, "$1,");
    else
        return num_str.substring(0, offset) + num_str.substring(offset).replace(/([0-9]{3})/g, ",$1");
}

function splitTextForNum(str, pos) {
  return parseInt(str.split(' ')[pos]);
}

$(document).ready(function() {
    // animate scrolls 
    $("#twtr-navbar-collapse ul li a[href^='/#'], #twtr-navbar-collapse ul li a[href^='#'],"+
        " a[href^='#'], a[href^='/#']").on('click', function(e) {
        if (window.location.pathname !== '/') {
          // we are off the main page
          if ($(e.currentTarget).attr('href')[0] !== '#') {
            // it's a link to another page
            return true;
            // redirect
          }
        } 
        // scroll
        e.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top-10
        }, 300, function(){
          // when done, add hash to url
          // (default click behaviour)
          window.location.hash = hash;
        });
    });

    // navbar collapse
    var navMain = $("#twtr-navbar-collapse");
    navMain.on("click", "a:not([data-toggle='dropdown'])", null, function () {
        navMain.collapse('hide');
    });

    // clock
    if ($('#clockdiv').length > 0) {
      var deadline = new Date(Date.parse(new Date("May 22, 2018 12:00:00")) );
      initializeClock('clockdiv', deadline);
    }

    // actionnetwork petition load
    $(document).on('can_embed_loaded', function() {
        // adjust petition status to include change.org signatures
        var running_goal_text = $('.action_status_running_total').text().replace(/,/g,'');
        var an_signers = splitTextForNum(running_goal_text, 0);
        var change_signers = 3666; // https://www.change.org/p/twitter-inc-vote-yes-to-a-co-op-with-your-users
        var total_signers = an_signers+change_signers;
        $('.action_status_running_total').text(addCommas(total_signers)+' Users Signed');
        
        // reads as "Only X until our goal of Y"
        var new_goal_text = $('.action_status_goal').text().trim().replace(/,/g,'');
        var new_goal_more = splitTextForNum(new_goal_text, 1) + 334;
        var new_goal_value = splitTextForNum(new_goal_text, 7) + 4000; // round goalpost up
        $('.action_status_goal').text("Only "+addCommas(new_goal_more)+" until our goal of "+addCommas(new_goal_value));

        var new_goal_pct = (total_signers/new_goal_value)*100.0;
        $('.action_status_status_bar > span').css('width', new_goal_pct+'%');
    });
});