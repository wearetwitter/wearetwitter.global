Thanks to people like you, we've come a long way!

Last Fall, reports came out that Twitter was seeking a buyer. People in the [#platformcoop](https://platform.coop) movement talked about a cooperative alternative, and started organizing around #BuyTwitter. Many of us feared Twitter's new owner might ruin "the people's news platform" with a narrow pursuit of profit or political gains. We proposed that Twitter sell to us, instead, with broad-based ownership and accountability. We've grown into a community of over 5,000 shareholders and users invested in Twitter's future.

On May 22, 2017, 4.9% of shareholder voted in favor of our proposal. We'll soon start developing a better proposal for next year, and exploring ways to start the study ourselves. We're also continuing our conversation with Twitter to move faster! And in parallel, we're experimenting with [social.coop](https://social.coop), our own instance of the free, open-source social network Mastadon.

Whatever happens, we’ll continue learning and advocating for democratic ownership of the platforms we use.
