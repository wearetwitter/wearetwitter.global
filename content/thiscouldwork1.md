# Ways this could work

Twitter is a vital public utility for news and social movements. CEO Jack Dorsey referred to Twitter as the “people’s news network,” but the company has struggled to capture value for shareholders.

We propose that Twitter study a possible exit or transition to democratic user ownership and report back with insights on user loyalty and overall shareholder value. Our proposal opens a conversation with Twitter, its shareholders, and its users about the benefits of democratic ownership and accountability, like a co-op.

To get things started, here are a few scenarios to consider:

### Scenario 1: Leveraged multi-stakeholder buyout

**Private equity leverages a transition on behalf of a trust that represents multiple Twitter stakeholder groups.**

Private equity firm Awesome Assets (AA), which specializes in large-scale impact investing, recognizes the added value that a user-owned Twitter could have—far more value than speculative markets can recognize. A Tweeters Trust (TT) forms, an entity designed to own the company on behalf of its users, as well as employees and founders. AA finances a full buyout of the company on behalf of TT, resulting in a satisfying windfall for anxious shareholders. TT begins its stewardship of the platform. Over ten years, TT fulfills its contract to repay AA with revenues based on users’ membership contributions, boutique user services, and advertising. Once that is paid off, users stand to receive dividends in the form of credits for boutique services on the platform.

To govern TT, Twitter users elect board members and decide on proposals, with their votes weighed, by account, on the basis of a Klout-like influence score. While all common stock is now held in TT, non-voting preferred shares still trade on public markets—at a value buoyed by the loyalty that user ownership and control engenders.

### Scenario 2: Partial restructuring

**Users gain some control and benefit in the company while shares continue to trade on public markets.**

Current shareholders, recognizing the value that user co-ownership would entail, aid in the creation of a Tweeters Trust (TT), which comes to hold a considerable portion of non-trading stock and control several board seats. This might take place through a new issuance or a partial buyout. Users determine how their board representatives vote on major decisions through sortition or a [liquid democracy](https://en.wikipedia.org/wiki/Delegative_democracy) system, which enables them to select provisional proxies to vote for them on particular kinds of decisions, or to vote for themselves on any proposal they wish.

Shares of the company not held in the trust continue to trade on public markets, with increased value due to renewed user interest, participation, and investment.

### Scenario 3: Media consortium

**A diverse set of media companies would have a common interest in maintaining Twitter as an open, sustainable platform.**

Cooperative associations of news organizations—such as Associated Press, Canadian Press, and Britain’s Press Association—have been an effective way of managing shared media utilities for nearly two centuries. In this way, a consortium of public and private media companies organize their own Tweeters Trust (TT) and buy the company together. Rather than seeking profits directly, they jointly invest in it and operate it at-cost as a common platform for reliable news gathering, circulation, and audience interaction. Member companies gain access to boutique analytics and advertising tools, though the platform’s data remains an open commons.

### Scenario 4: Crypto-token issuance

**A bold experiment in blockchain-based finance and governance adds a new layer to the existing Twitter ownership stack.**

Twitter devises a cryptocurrency-based token system for users, picking up where the [Reddit Notes](https://redditblog.com/2015/12/19/announcing-reddit-notes/) proposal left off, and following [a suggestion](http://avc.com/2016/07/the-golden-age-of-open-protocols/) by early investor Fred Wilson. A new stock issuance results in Tweeters Trust (TT), whose value backs BirdieTokens (BT), a cryptocurrency continuously distributed among users for their use of the platform. A cryptocurrency wallet becomes a built-in feature of Twitter, allowing for micropayments in BTs, Bitcoin, and more. This system becomes a new source of revenue. Holding BirdieTokens, also, results in voting rights for the board seat and proxy votes the TT controls.
