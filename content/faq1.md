#### 1. Can't users buy Twitter stock already?

We mean "buy" as in, buy-out or acquisition.

#### 2. How could an exit to democratic ownership work?

It's complicated. That's why we propose Twitter do due diligence with a full study.

#### 3. Okay! But won't democratic ownership lower stock prices?

We see many ways a more cooperative model would increase Twitter's value.

#### 4. What about trolls?

We LOL at trolls.

#### 5. I'm in. How can I help?

Shareholders should vote [YES on Proposal 4](/vote).

Co-ops and organizations can [sign our letter](/coopallies).

Users and everyone, please [sign our petition](/#petition)!

[Read full FAQ](/faq/){class="btn btn-block btn-lg btn-twtr centered"}