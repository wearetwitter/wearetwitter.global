We’re glad you asked!

Here are five reasons why democratic ownership could be better than Twitter’s current model:

#### 1) Democratic ownership has a proven record of profitability and sustainability.

Cooperative ownership models that engage their members create lasting benefits. For example:

*   **The Associated Press** – Founded as a cooperative of newspapers in the 1840s, AP carries a standard for real news, despite media disruption and polarization.
*   **REI** – Offering co-op ownership to customers since 1938, this outdoor goods chain has won their loyalty and continues to buck the trend of declining retail sales, on and offline.
*   **The Green Bay Packers** – After the team began selling shares to fans in 1920, it built a robust franchise and fanbase instead of relocating to bigger markets.
*   **Futbol Club Barcelona** – Founded in 1899, this world-famous soccer (football) club in Spain is a cooperative that is owned by the club’s loyal fan base.

These models take different forms, and we believe there are several kinds of forms that user-ownership of Twitter could take.

#### 2) A co-op or hybrid would benefit Twitter users and shareholders.

Modern, industrial cooperative enterprises have demonstrated powerful competitive advantages that democratic business models offer.

*   From rural electricity to family-owned hardware stores, cooperatives have a knack for **addressing market failure** and creating durable models for common-good businesses that speculative markets fail to support.
*   **User commitment** that stems from real co-ownership can be far more powerful than any “sense of ownership” an investor-owned company can offer. Companies owned by their employees, for instance, tend to be more productive than their competitors.
*   When a company’s users are also its owners, **information sharing** can happen more freely; users feel more trust in sharing information about themselves, and the company can be more transparent in what it shares with its users.
*   Because of their committed stakeholdership, cooperatives tend to have **lower rates of failure** than other kinds of businesses.
*   A more diverse set of stakeholders, with more varied incentive structures than investor-owners, can result in greater **resilience** for cooperative businesses.
*   Replacing client relationships with ownership relationships makes possible kinds of **vertical integration** that investor-owned companies can only access at much greater expense.

#### 3) Studying models for user ownership will show how current shareholders stand to gain.

Our proposal calls on Twitter to study options for building user ownership into its business model, with current shareholders reaping the rewards. Key questions about how shareholders might benefit include:

*   What additional revenue and capital opportunities could shared ownership present?
*   How would a leveraged buyout strategy compare to new stock issuance?
*   Would it be more advantageous to seek a full buyout or a partial one?
*   Could preferred shares be used to create a hybrid business with the advantages of a cooperative but allowing Twitter to continue as a publicly traded company?
*   What role could founders and employees play as co-owners along with users in order to align their interests?
*   What techniques for shared governance might ensure accountable, flexible management?

#### 4) To evaluate opportunities, Twitter can engage a range of experts in democratic ownership.

Our proposal asks Twitter to “engage consultants with significant experience in corporate governance, preferably including conversion of companies to cooperatives or employee ownership” in order to explore more inclusive ownership models. Several highly qualified organizations we might consider include:

*   **McKinsey & Co.** consultants have published extensive research on [opportunities for co-ops worldwide](http://www.mckinsey.com/~/media/mckinsey/dotcom/client_service/strategy/mckinsey%20on%20cooperatives/pdfs/mck_on_cooperatives-full_issue.ashx).
*   **Bain and Company** consultants have [produced a report](https://www.sommetinter.coop/en/virtual-library/studies/member-driven-strategies-leveraging-cooperatives-competitive-advantage) on leveraging the competitive advantages of cooperative membership.
*   **Deloitte** has examined [financing opportunities](https://www.sommetinter.coop/fr/cdn/farfuture/JHDH7HH8mQh8ATRk9w48JKHkBrDo87qqfbMrYewvR5k/mtime%3A1435673870/sites/default/files/etude/files/deloitte_funding_the_future.pdf) for cooperative businesses.
*   **Ernst & Young** has [studied cooperative governance models](https://www.sommetinter.coop/fr/cdn/farfuture/iO7a_SUri73ZW_DQ22fCLkIJvlpeML0yW1vcc0mk9iU/mtime%3A1435674034/sites/default/files/etude/files/ey_study_report_governance.pdf), emphasizing the competitive advantages of co-ops.
*   [ICA Group](http://ica-group.org/) is a non-profit with decades of experience with the assessment and conversion of companies to broad-based ownership.
*   The [International Co-operative Alliance](http://ica.coop/) represents the global co-op movement.
*   The [National Cooperative Business Association](http://ncba.coop/) is the U.S. umbrella organization for cooperative enterprises.
*   The [Platform Cooperativism Consortium](http://platformcoop.newschool.edu/) at The New School is devoted to research on cooperative models online.
*   The [Sustainable Economies Law Center](http://theselc.org/) is an authority on shared ownership models for a real sharing economy.

#### 5) This proposal emerged from organizing among Twitter shareholders and users.

*   #WeAreTwitter #BuyTwitter is a community invested in the platform’s future.
*   We began organizing around #WeAreTwitter and #BuyTwitter in Fall 2016, following reports suggesting Twitter—along with its users and their data—might be up for sale.
*   Since then, more than 5,000 individuals (including more than 500 shareholders) have been organizing online to develop better, more creative options for the company and its shareholders than the standard corporate acquisition. Several recognized experts have joined our effort, including Jim McRitchie, publisher of [corpgov.net](http://www.corpgov.net/) and a co-author on our shareholder resolution.
*   Our efforts have been featured in [_The Guardian_](https://www.theguardian.com/commentisfree/2016/sep/29/save-twitter-buy-platform-shared-ownership), [_WIRED_](https://www.wired.com/2016/11/lets-build-next-twitter-like-green-bay-packers/), [_NPR_](http://www.wqxr.org/story/the-takeaway-2016-11-08/), [_The Financial Times_](https://www.ft.com/content/73da6746-e8c9-11e6-893c-082c54a7f539) [twice](https://www.ft.com/content/5997d76e-ede3-11e6-ba01-119a44939bb6), [_Recode_](https://www.recode.net/2017/4/9/15234376/twitter-acquisition-sale-co-op), and nearly 100 more articles worldwide - see all press [here](/press/).
*   Learn more on this site and say hello to [@BuyThisPlatform](https://twitter.com/BuyThisPlatform).
