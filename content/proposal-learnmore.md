After an op-ed by Nathan Schneider caught a wave of attention, we ran an online petition that picked up a wave of positive attention.

Twitter didn’t respond, so, we decided to get serious and hatch a shareholder resolution. We submitted the proposal above on December 16, 2016 for Twitter’s next general meeting. As expected, Twitter’s general counsel tried to block the proposal, but we submitted a rebuttal and the SEC ruled that Twitter shareholders should take it to a vote.

Voting is open now until May 22, 2017.

Whatever happens, we’ll continue learning and advocating for democratic ownership of the platforms we use.

Want to read more? Browse the [legal proposal documents](https://drive.google.com/drive/folders/0B96fUd3d-oRwRHRBUEF1eXdnN1E){target=blank}, skim [101 articles about #BuyTwitter](/press/), and read our analysis on [Ways This Could Work](/thiscouldwork).

Want to get updates? [Follow @BuyThisPlatform](https://twitter.com/BuyThisPlatform).
