## Exit to Democratic User Ownership - Proposal 4

<div class="row">
	<div class="col-xs-12 center-all">
	  <p class="triangle-border center-bottom warm">Below is the shareholder resolution going up for a vote</p>
	  <img class="img-responsive" src="img/chickemoji-xs.png">
	</div>
</div>

As Twitter users and stockholders, we see how vital the platform is for global media. We are among millions of users that value Twitter as a platform for democratic voice. And in 2016, we’ve seen Twitter’s future in the balance, from challenges of hate speech and abuse to the prospect of a buyout.

That is why we want the company to consider more fully aligning its future with those whose participation make it so valuable: its users. As of today, 3,300 individuals signed a petition at wearetwitter.global urging Twitter to build democratic user ownership. For successful enterprises like the Green Bay Packers, REI, and the Associated Press, their popularity, resilience, and profitability is a result of their ownership structure. Examples of online companies include successful startups like Managed by Q, which allocates equity to office cleaners, and Stocksy United, a stock-photo platform owned by its photographers.

We believe these models point the way forward for Twitter, Inc., overcoming challenges to thrive as a cooperative platform.

A community-owned Twitter could result in new and reliable revenue streams, since we, as users, could buy in as co-owners, with a stake in the platform’s success. Without the short-term pressure of the stock markets, we can realize Twitter’s potential value, which the current business model has struggled to do for many years. We could set more transparent, accountable rules for handling abuse. We could re-open the platform’s data to spur innovation. Overall, we’d all be invested in Twitter’s success and sustainability. Such a conversion could also ensure a fairer return for the company’s existing investors than other options.Therefore,

RESOLVED: Stockholders request that Twitter, Inc. engage consultants with significant experience in corporate governance, preferably including conversion of companies to cooperatives or employee ownership, to prepare a report on the nature and feasibility of selling the platform to its users via a cooperative or similar structure with broad-based ownership and accountability mechanisms. The requested report shall be available to stockholders and investors by October 1, 2017, prepared at reasonable cost and omitting proprietary information.

[Vote Yes](/#vote){class="btn btn-cta btn-medium centered"}

[Read ways this could work](/thiscouldwork/){class="centered"}
