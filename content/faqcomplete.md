#### 1. Can't users buy Twitter stock already?

We mean "buy" as in, buy-out or acquisition.

But, we're proposing a step before that – our shareholder resolution asks Twitter to explore models through which it could sell to its users and other stakeholders, with broad-based ownership and accountability.

Once we have insights on the feasibility of this exit/transition, Twitter can take next step of offering users, employees, and shareholders a way to "buy in" to a more democratically owned and governed platform.

#### 2. How could an exit to democratic ownership work?

It's complicated. That's why we propose Twitter do due diligence with a full study.

But, in the spirit of cooperation, we've thought up several scenarios to explore: a full buy-out that creates a user's trust, a partial buy-out that puts users on the board, a news consortium, and, yes, a crypto-token issuance.

[Read ways this could work](/thiscouldwork/) for four scenarios and five reasons why #BuyTwitter is in everyone's financial interest.

#### 3. Okay! But won't democratic ownership lower stock prices?

We see many ways a more cooperative model would increase Twitter's value.

For example: user trust and loyalty, buy-in as co-owners, and good governance. Democratic ownership has a long history of profitability and sustainability. Reports by McKinsey, Deloitte, EY, and Bain found competitive advantages of cooperative [enterprise](http://bccm.coop/wp/wp-content/uploads/2015/03/McK_on_Cooperatives-Full_issue.pdf){target="_blank"}, [finance](https://www.sommetinter.coop/fr/cdn/farfuture/JHDH7HH8mQh8ATRk9w48JKHkBrDo87qqfbMrYewvR5k/mtime%3A1435673870/sites/default/files/etude/files/deloitte_funding_the_future.pdf){target="_blank"}, [governance](https://www.sommetinter.coop/fr/cdn/farfuture/iO7a_SUri73ZW_DQ22fCLkIJvlpeML0yW1vcc0mk9iU/mtime%3A1435674034/sites/default/files/etude/files/ey_study_report_governance.pdf){target="_blank"}, and [member experience](https://www.sommetinter.coop/en/virtual-library/studies/member-driven-strategies-leveraging-cooperatives-competitive-advantage){target="_blank"}.

By doing its own study on democratic ownership, Twitter can learn from REI, Associated Press, and other enterprises that have thrived for decades. Twitter can also evaluate exits or transitions that continue to deliver shareholder value, for example, with preferred shares that still trade publicly.

Our proposal asks Twitter to study all of this, and report back publicly with insights.

#### 4. What about trolls?

We LOL at trolls.

But seriously, several volunteers organizing with #WeAreTwitter are developing proposals to overcome evil. We discussed many ideas in [a recent webinar](https://twitter.com/BuyThisPlatform/status/852552496539025408){target="_blank"} with governance experts. A few solutions so far include, policies to keep the platform free and open, like a public utility; effective moderation to block harassment, hate speech, and bullying; and improved accountability through electing users to the board; and "sortition," a process of selecting a representative set of stakeholders to weigh in on decisions.

As die-hard Twitter users and shareholders, we have faith that everyone invested in the platform can steward its value into the future.

#### 5. I'm in. How can I help?

Shareholders should vote [YES on Proposal 4](/vote).

Co-ops and organizations can [sign our letter](/coopallies).

Users and everyone, please [sign our petition](/#petition)!
