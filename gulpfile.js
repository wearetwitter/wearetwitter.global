var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    // cssnext     = require("gulp-cssnext"),
    postcss     = require("gulp-postcss"),
    cssreset    = require('postcss-css-reset'),
    cssnext     = require('postcss-cssnext'),
    sourcemaps  = require('gulp-sourcemaps'),
    uglify      = require('gulp-uglify'),
    pug         = require('gulp-pug'),
    template    = require('gulp-template'),
    concat      = require('gulp-concat'),
    indexify    = require('gulp-indexify'),
    livereload  = require('gulp-livereload'), // Livereload plugin needed: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
    tinylr      = require('tiny-lr'),
    express     = require('express'),
    app         = express(),
    path        = require('path'),
    server      = tinylr(),
    image       = require('gulp-image'),
    config      = require('./config.js');

var awspublish = require('gulp-awspublish'),
    cloudfront = require('gulp-cloudfront-invalidate-aws-publish');
var postcssImport = require("postcss-import");

// --- Basic Tasks ---
gulp.task('css', function() {
  var processors = [
    postcssImport(),
    cssreset(),
    cssnext({browsers: ['last 1 version']})
  ];
  return gulp.src('content/assets/css/styles.css')
  .pipe(sourcemaps.init())
  .pipe(postcss(processors))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('dist/css'))
  .pipe(livereload(server));
});

gulp.task('js', function(done) {
  gulp.src([
      'content/assets/js/wearetwitter.js',
    ])
    .pipe(template(config))
    .pipe(uglify())
    .pipe(concat('site.min.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(livereload(server));
  gulp.src([
      'content/assets/js/modernizr.js',
      'content/assets/js/jquery-1.11.2.min.js',
      'content/assets/js/bootstrap-3.3.2.js',
    ])
    .pipe(uglify())
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('dist/js'));
  done();
});

gulp.task('templates', function(done) {
  var pages = gulp.src('content/*.pug')
    .pipe(pug({
      pretty: true,
      locals: config
    }))
    .pipe(template(config))
    .pipe(gulp.dest('dist/'))
    .pipe(livereload(server));
  pages.pipe(indexify({rewriteRelativeUrls: true}))
    .pipe(gulp.dest('dist/'));
  done();
});

gulp.task('images', function(done) {
  gulp.src(['content/assets/img/*.jpg'])
      .pipe(image())
      .pipe(gulp.dest('dist/img'));
  gulp.src(['content/assets/img/*.png', 'content/assets/img/*.pdf', 'content/assets/img/*.gif'])
      .pipe(gulp.dest('dist/img'));
  done();
});

gulp.task('express', function(done) {
  app.use(require('connect-livereload')());
  app.use(express.static(path.resolve('./dist')));
  app.listen(config.port);
  gutil.log('Listening on port: ' + config.port);
  done();
});

gulp.task('watch', function (done) {
  livereload.listen();
  gulp.watch('content/assets/img/**/*.*',gulp.series('images'));
  gulp.watch('content/assets/css/**/*.css',gulp.series('css'));
  gulp.watch('content/assets/js/*.js',gulp.series('js'));
  gulp.watch('content/includes/*.*',gulp.series('templates'));
  gulp.watch('content/*.*',gulp.series('templates'));
  done();
});

gulp.task('css:prod', function(done) {
  var processors = [
    cssreset(),
    postcssImport(),
    cssnext({browsers: ['last 1 version']}),
    // opacity,
  ];
  return gulp.src('content/assets/css/styles.css')
  .pipe(postcss(processors))
  .pipe(gulp.dest('dist/css'))
});

// create a new publisher using S3 options
// http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
var publisher = awspublish.create({
  region: config.awsRegion,
  params: {
    Bucket: config.awsBucket
  }
}, {
  cacheFileName: config.awsCache
});

// define custom headers
var headers = {
  // 'Cache-Control': 'max-age=315360000, no-transform, public'
  // ...
};

gulp.task('publish', function() {
  return gulp.src('./dist/**/*')
     // gzip, Set Content-Encoding headers and add .gz extension
    // .pipe(awspublish.gzip({ ext: '.gz' }))

    // publisher will add Content-Length, Content-Type and headers specified above
    // If not specified it will set x-amz-acl to public-read by default
    // .pipe(publisher.publish(headers))
    .pipe(publisher.publish())

    // create a cache file to speed up consecutive uploads
    .pipe(publisher.cache())

     // print upload updates to console
    .pipe(awspublish.reporter());
});

var cfSettings = {
  distribution: config.cloudFrontDistID, // Cloudfront distribution ID
  wait: true,                            // Whether to wait until invalidation is completed (default: false)
  indexRootPath: true                    // Invalidate index.html root paths (`foo/index.html` and `foo/`) (default: false)
}

gulp.task('invalidate', function () {
  return gulp.src('./dist/**/*')
    .pipe(publisher.publish())
    .pipe(cloudfront(cfSettings))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());
});

gulp.task('build', gulp.series('js','css:prod','templates','images'))

gulp.task('deploy', gulp.series('build', function(done) {
  gulp.start('invalidate');
  done();
}));


// Default Task
gulp.task('default', gulp.series('js','css','templates', 'images', 'express', 'watch'));
